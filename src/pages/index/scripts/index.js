import '../../../styles/index.scss';
import dominosArray from './data/dominos.json';
import mcArray from './data/mac.json';
import kfcArray from './data/kfc.json';

const domBtn = document.getElementById('dom');
const macBtn = document.getElementById('mac');
const kfcBtn = document.getElementById('kfc');
const container = document.getElementById('container');

class Dish {
    #count = 0
    constructor({id, price, title, img, count}) {
        this.id = id;
        this.price = price;
        this.title = title;
        this.img = img;
        this.#count = count;
    }

    getCount() {
        return this.#count;
    }

    setCount(number) {
        if(typeof number !== 'number' || number < 0) {
            throw new Error('count cannot be less than zero');
        }
        return this.#count = number;
    }

    render() {
        return `
            <div class="dish">
                <img class="dish__image" src="${this.img}" alt="${this.title}">
                <div class="dish__title">${this.title}</div>
                <div class="dish__info">
                    <div class="dish__price">${this.price} грн</div>
                    <div class="counter">
                        <button class="counter__button counter__button--increase"></button>
                        <span class="counter__number">${this.#count}</span>
                        <button class="counter__button counter__button--increase"></button>
                    </div>
                </div>
            </div>
        `;
    }
}

const domArr = dominosArray.map((item) => new Dish(item));
const mcArr = mcArray.map((item) => new Dish(item));
const kfcArr = kfcArray.map((item) => new Dish(item));

//устанавливаем по дефолту 
domBtn.classList.add('active');
const renderDom = domArr.map((item) => item.render()).join('');
container.innerHTML = '';
container.insertAdjacentHTML('afterBegin', renderDom);

domBtn.addEventListener('click', (e)=> {
    e.preventDefault();
    domBtn.classList.add('active');
    if(macBtn.classList.contains('active') || kfc.classList.contains('active')) {
        macBtn.classList.remove('active');
        kfcBtn.classList.remove('active');
    }

    container.innerHTML = '';
    container.insertAdjacentHTML('afterBegin', renderDom);
});

macBtn.addEventListener('click', (e)=> {
    e.preventDefault();
    macBtn.classList.add('active');
    if(domBtn.classList.contains('active') || kfc.classList.contains('active')) {
        domBtn.classList.remove('active');
        kfcBtn.classList.remove('active');
    }
    const renderMac = mcArr.map((item) => item.render()).join('');
    container.innerHTML = '';
    container.insertAdjacentHTML('afterBegin', renderMac);

});

kfcBtn.addEventListener('click', (e)=> {
    e.preventDefault();
    kfcBtn.classList.add('active');
    if(domBtn.classList.contains('active') || macBtn.classList.contains('active')) {
        domBtn.classList.remove('active');
        macBtn.classList.remove('active');
    }

    const renderKfc = kfcArr.map((item) => item.render()).join('');
    container.innerHTML = '';
    container.insertAdjacentHTML('afterBegin', renderKfc);
});

// let drawContent 